@extends('layouts.app')
@section('content')
    <div class="content">

        {!! Form::open(['route' => ['post.store'], 'method' => 'post', 'files' => true]) !!} 
        @include('post._form')
        {{ Form::submit('Criar post')}}
        {!! Form::close() !!}       

    </div>


@stop
