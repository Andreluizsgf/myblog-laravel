@extends('layouts.app')

@section('css')
	<style>i.glyphicon-plus-sign{font-size:30px; padding-right: 5%;}</style>
@stop

@section('content')
<h1>Todas as postagens <a href="post/create"><i class="btn-primary btn glyphicon glyphicon-plus-sign pull-right">Create</i></a></h1>

<table class="table">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">title</th>
            <th scope="col">text</th>
            <th scope="col">publish_time</th>
            <th width="5%" class="removesorting"></th>
            <th width="5%" class="removesorting"></th>
      
        </tr>
    </thead>
        <tbody class="table table-bordered table-striped">
            @foreach( $posts as $post )
                <tr>
                    <th scope="row">{{ $post->id }}</th>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->text }}</td>
                    <td>{{ $post->publish_time }}</td>
                    <td><a href="{{ route('post.show', $post->id) }}" class="btn btn-success">Read</a></td>
                    <td><a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary">Update</a></td>
                    <td>
                        <form action="{{ route('post.destroy', $post->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
</table>

@stop