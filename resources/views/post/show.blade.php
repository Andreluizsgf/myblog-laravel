@extends('layouts.app')
@section('content')
    <div class="content">

    <table class="table">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">title</th>
            <th scope="col">text</th>
            <th scope="col">publish_time</th>
            <th width="5%" class="removesorting"></th>
            <th width="5%" class="removesorting"></th>
      
        </tr>
    </thead>
        <tbody class="table table-bordered table-striped">

                <tr>
                    <th scope="row">{{ $post->id }}</th>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->text }}</td>
                    <td>{{ $post->publish_time }}</td>
                    <td><a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary">Update</a></td>
                </tr>
        </tbody>
</table>

    </div>


@stop