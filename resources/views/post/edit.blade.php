@extends('layouts.app')
@section('content')
    <div class="content">

        {!! Form::open(['route' => ['post.update', $post], 'method' => 'put', 'files' => true]) !!} 
        @include('post._form')
        {{ Form::submit('Editar Post')}}
        {!! Form::close() !!}

    </div>


@stop