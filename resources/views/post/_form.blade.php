<div class="form-group has-feedback {{ $errors->has('title') ? 'has-error' : '' }}">
    {{ Form::label('title', 'title*') }}
    {{ Form::text('title', null, ['class' => 'form-control', 'autofocus' => 'true', 'placeholder' => 'Digite seu nome completo', 'value' => $post->title ?? '']) }} 
    <span class="glyphicon glyphicon-user form-control-feedback"></span>
    @if ($errors->has('title'))
    <span class="help-block">
        <strong>{{ $errors->first('title') }}</strong>
    </span>
    @endif
</div>

<div class="form-group has-feedback {{ $errors->has('title') ? 'has-error' : '' }}">
    {{ Form::label('text', 'text*') }}
    {{ Form::text('text', null, ['class' => 'form-control', 'autofocus' => 'true', 'placeholder' => 'Digite seu nome completo', 'value' => $post->text ?? '']) }} 
    <span class="glyphicon glyphicon-user form-control-feedback"></span>
    @if ($errors->has('text'))
    <span class="help-block">
        <strong>{{ $errors->first('text') }}</strong>
    </span>
    @endif
</div>

<div class="form-group has-feedback {{ $errors->has('publish_time') ? 'has-error' : '' }}">
    {{ Form::label('publish_time', 'publish_time*') }}
    {{ Form::date('publish_time', null, ['class' => 'form-control', 'placeholder' => 'publish_time', 'value' => $post->publish_time ?? '']) }}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    @if ($errors->has('publish_time'))
    <span class="help-block">
        <strong>{{ $errors->first('publish_time') }}</strong>
    </span>
    @endif
</div>