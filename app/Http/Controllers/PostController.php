<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

          $post = new Post;
          $post->title = $request->title;
          $post->text = $request->text;
          $post->publish_time = $request->publish_time;
          $post->user_id = 1;
          $post->save();
        
        $post->save();
        return redirect()->route('post.index')->with('sucess','Novo post criado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($post)
    {  
        $post = Post::find($post);
        //dd($post);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($post)
    {
        return view('post.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post)
    {
        //dd($post);
        $post = Post::find($post);
        //dd($post);
        $post->title = $request->title;
        $post->text = $request->text;
        $post->publish_time = $request->publish_time;
        $post->user_id = 1;
        $post->save();
        return redirect()->route('post.index')->with('sucess','Novo post criado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post)
    {
        $post = Post::find($post);
        $post->delete();
        return redirect()->route('post.index')->with('sucess','Novo post criado!');

    }
}
